/*----- PROTECTED REGION ID(PrevacVCHStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        PrevacVCHStateMachine.cpp
//
// description : State machine file for the PrevacVCH class
//
// project :     PREVAC_VCH01
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include "PrevacVCH.h"

/*----- PROTECTED REGION END -----*/	//	PrevacVCH::PrevacVCHStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================


namespace PrevacVCH_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : PrevacVCH::is_brightness_allowed()
 *	Description : Execution allowed for brightness attribute
 */
//--------------------------------------------------------
bool PrevacVCH::is_brightness_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for brightness attribute in Write access.
	/*----- PROTECTED REGION ID(PrevacVCH::brightnessStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PrevacVCH::brightnessStateAllowed_WRITE

	//	Not any excluded states for brightness attribute in read access.
	/*----- PROTECTED REGION ID(PrevacVCH::brightnessStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PrevacVCH::brightnessStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : PrevacVCH::is_time_allowed()
 *	Description : Execution allowed for time attribute
 */
//--------------------------------------------------------
bool PrevacVCH::is_time_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for time attribute in Write access.
	/*----- PROTECTED REGION ID(PrevacVCH::timeStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PrevacVCH::timeStateAllowed_WRITE

	//	Not any excluded states for time attribute in read access.
	/*----- PROTECTED REGION ID(PrevacVCH::timeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PrevacVCH::timeStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : PrevacVCH::is_Off_allowed()
 *	Description : Execution allowed for Off attribute
 */
//--------------------------------------------------------
bool PrevacVCH::is_Off_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Off command.
	/*----- PROTECTED REGION ID(PrevacVCH::OffStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PrevacVCH::OffStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : PrevacVCH::is_On_allowed()
 *	Description : Execution allowed for On attribute
 */
//--------------------------------------------------------
bool PrevacVCH::is_On_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for On command.
	/*----- PROTECTED REGION ID(PrevacVCH::OnStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PrevacVCH::OnStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : PrevacVCH::is_ClearErrors_allowed()
 *	Description : Execution allowed for ClearErrors attribute
 */
//--------------------------------------------------------
bool PrevacVCH::is_ClearErrors_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ClearErrors command.
	/*----- PROTECTED REGION ID(PrevacVCH::ClearErrorsStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	PrevacVCH::ClearErrorsStateAllowed
	return true;
}


/*----- PROTECTED REGION ID(PrevacVCH::PrevacVCHStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	PrevacVCH::PrevacVCHStateAllowed.AdditionalMethods

}	//	End of namespace
