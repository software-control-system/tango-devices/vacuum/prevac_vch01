//- Project : PREVAC Protocol class

#include <string.h>
#include "PrevacProtocol.h"

namespace prevac
{
  //-------------------------------------------------------
  //- Config structure
  //- CTOR, default configuration 
  //-------------------------------------------------------
  Config::Config () :
    header (0xAA),
    data_length (0x05),
    device_address (0xC8),
    device_group (0x99),
    logic_group (0xC8),
    driver_address (0x01),
    function_code (0x53)
  {
    //- NOOP CTOR
  }
  
  //-------------------------------------------------------
  //- copy CTOR
  //-------------------------------------------------------
  Config::Config ( const Config & c)
  {
    header  = c.header;
    data_length = c.data_length;
    device_address = c.device_address;
    device_group = c.device_group;
    logic_group = c.logic_group;
    driver_address = c.driver_address;
    function_code = c.function_code;
  }
  //-------------------------------------------------------

  //-------------------------------------------------------
  //- PrevacProtocol DTOR
  //-------------------------------------------------------
  PrevacProtocol::~PrevacProtocol ()
  {
    // NOOP
  }

  //-------------------------------------------------------
  //- PrevacProtocol CTOR
  //-------------------------------------------------------
  PrevacProtocol::PrevacProtocol (const Config & cnf) :
  conf (cnf)
  {
    ::memset (request, 0, (8 + 256));
    ::memset (response, 0, (8 + 256));
    request [HEADER_BYTE_OFFSET] = conf.header;
    request [DATA_LEN_BYTE_OFFSET] = conf.data_length;
    request [REQUEST_DEV_ADDR_BYTE_OFFSET] = conf.device_address;
    request [DEV_GROUP_BYTE_OFFSET] = conf.device_group;
    request [LOG_GROUP_BYTE_OFFSET] = conf.logic_group;
    request [REQUEST_DRV_ADDR_BYTE_OFFSET] = conf.driver_address;
    request [FUNC_CODE_BYTE_OFFSET] = conf.function_code;
  }

  //------------------------------------------------------------------------
  //- prepare the request (set the data to send)
  void PrevacProtocol::set_req_data (unsigned char len, unsigned char * dat)
  {
    //- clear the bytes after the 7 header bytes
    ::memset (request + 7, 0, (256 + 1));
    //- set the data length
    request [DATA_LEN_BYTE_OFFSET] = len;
    //- copy the data
    for (std::size_t i = 0; i < len; i ++)
    {
      request [DATA_START_ADDR_BYTE_OFFSET + i] = dat [i];
    }
    //- compute the CRC (sum of all bytes begining with data length byte, ending at last data byte)
    //- to be put in the byte immediately following the las data byte
    unsigned long crc = 0;
    std::size_t length = (7 + request [DATA_LEN_BYTE_OFFSET]);
    for (std::size_t i = 1; i < length; i++)
    {
      crc = ((request [i] + crc) % 256);
    }
    request [7 + request [DATA_LEN_BYTE_OFFSET]] = static_cast <unsigned char> (crc);
  }

  //------------------------------------------------------------------------
  //_ get_req_length
  //------------------------------------------------------------------------
  int PrevacProtocol::get_req_length ()
  {
	  //- header size + data length + crc byte
    return 7 + request [DATA_LEN_BYTE_OFFSET] + 1;
  }

} // namespace
