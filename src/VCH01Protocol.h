//- Project : PREVAC Protocol class
//- file : VCH01 implementation

#ifndef __VCH01_PROTOCOL_H__
#define __VCH01_PROTOCOL_H__


#include "PrevacProtocol.h"
#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/threading/Atomic.h>


namespace prevac
{

class Vch01Protocol: public PrevacProtocol,public yat4tango::DeviceTask
{

public : 

  Vch01Protocol (Tango::DeviceImpl* host_dev,
                 std::string ser_proxy,
                 const Config & conf);
  ~Vch01Protocol ();

  void on ();
  void off (void);
  void set_brightness (unsigned short val);
  void set_time_mn (unsigned short val);
  unsigned short get_brightness (void);
  unsigned short get_time_mn (void);
  bool is_on (void);
  std::string get_error() {
    return m_error;
  }
  void clear_error() {
    m_error = "";
  }

protected://- [yat4tango::DeviceTask implementation]
  virtual void process_message( yat::Message& msg );

 private : 
  void update_device_status (void);
  //- write a frame on serial proxy to the CVH01
  void write_frame (void);
  //- read a frame on the serial proxy to the CVH01
  int read_frame  (void);
  void display_request (void);
  void display_response();
  
  Config conf;
  yat::UniquePtr<Tango::DeviceProxy> m_serial;
  std::string m_proxy_name;
  yat::Atomic<std::size_t> m_time_remaining_read;
  yat::Atomic<std::size_t> m_brightness_read;
  yat::Atomic<std::string> m_error;
  unsigned short m_time_remaining_write;
  unsigned short m_brightness_write;
};

} //- namespace

#endif
