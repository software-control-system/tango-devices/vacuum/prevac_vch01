//- Project : PREVAC Protocol class
//- file : VCH01 implementation

#include "VCH01Protocol.h"
#include <iomanip>
#include <iostream>
#include <yat/time/Time.h>
#include <vector>

namespace prevac
{
//- Task period
const std::size_t    MESSAGE_PERIOD  =  1100; //- need ~1000ms to update data from HW!

//- commands
const std::size_t    MSG_COMMAND_ON  =  yat::FIRST_USER_MSG + 10;
const std::size_t    MSG_COMMAND_OFF =  yat::FIRST_USER_MSG + 11;

//- data
const std::size_t    MSG_SET_BRIGHTNESS =  yat::FIRST_USER_MSG + 20;
const std::size_t    MSG_SET_TIME_MN    =  yat::FIRST_USER_MSG + 21;

//-----------------------------------------
//- Vch01Protocol::Vch01Protocol 
//-----------------------------------------
Vch01Protocol::Vch01Protocol (Tango::DeviceImpl* host_dev, std::string serial_proxy, const Config & cnf) : 
  PrevacProtocol (cnf),
  yat4tango::DeviceTask(host_dev),
  m_serial (0),
  m_proxy_name(serial_proxy),
  m_time_remaining_read (0),
  m_brightness_read (0),
  m_error(""),
  m_time_remaining_write (0),
  m_brightness_write (0)
{
  //- Noop
}

//-----------------------------------------
//- Vch01Protocol::~Vch01Protocol 
//-----------------------------------------
Vch01Protocol::~Vch01Protocol ()
{
  m_serial.reset();
}

//+----------------------------------------------------------------------------
//
// Protected method :       Vch01Protocol::process_message
//
// description :            Implementation of a pure virtual method from yat4tango::DeviceTask
//
//-----------------------------------------------------------------------------
void Vch01Protocol::process_message(yat::Message& _msg)
{
    try
    {
        switch ( _msg.type() )
        {
            unsigned char data [5];
        //-----------------------------------------------------------------------------------------------
            case yat::TASK_INIT:
              std::clog << "\n\n\t Vch01Protocol::TASK_INIT entering..." << std::endl;
                try
                {
                  yat::Timer t;
                  //- create device proxy on serial line
                  m_serial.reset(new Tango::DeviceProxy (m_proxy_name.c_str ()));
                  m_serial->set_transparency_reconnection (true);
                  m_serial->ping ();

                  //- configure to 57600,n,8,1
                  //- baud 57600
                  Tango::DeviceData ddbaud;
                  ddbaud << static_cast <Tango::DevULong> (57600);
                  m_serial->command_inout ("DevSerSetBaudrate", ddbaud);

                  //- parity NONE
                  Tango::DeviceData ddpar;
                  ddpar << static_cast <Tango::DevShort> (0);
                  m_serial->command_inout ("DevSerSetParity", ddpar);

                  //- NB bits 8
                  Tango::DeviceData ddchrl;
                  ddchrl << static_cast <Tango::DevShort> (0);
                  m_serial->command_inout ("DevSerSetCharLength", ddchrl);

                  //- Stop bits 1
                  Tango::DeviceData ddstpb;
                  ddstpb << static_cast <Tango::DevShort> (0);
                  m_serial->command_inout ("DevSerSetStopbit", ddstpb);

                  //- get the device status
                  update_device_status ();
                  m_time_remaining_write = m_time_remaining_read;
                  m_brightness_write = m_brightness_read;

                  //- configure task
                  yat4tango::DeviceTask::enable_timeout_msg(false);   //- No need of TASK_TIMEOUT message type
                  yat4tango::DeviceTask::enable_periodic_msg(true);   //- Need of TASK_PERIODIC message type
                  yat4tango::DeviceTask::set_periodic_msg_period(MESSAGE_PERIOD);
                
                }
                catch(Tango::DevFailed & df)
                {
                  FATAL_STREAM << "Vch01Protocol failed to create/configure proxy!" << std::endl;
                  FATAL_STREAM << df << std::endl;
                  m_error = std::string (df.errors[0].desc);
                  m_serial.reset();
                }

                break;

        //-----------------------------------------------------------------------------------------------
            //- GETTING CASE, Called periodically
            case yat::TASK_PERIODIC:
                
                update_device_status();
                
                break;

        //-----------------------------------------------------------------------------------------------
            case MSG_COMMAND_ON:

                req_set_function_code (WRITE_PARAMETERS);
                data [0] = 0x01;
                data [1] = ((m_time_remaining_write  & 0xFF00) >> 8);
                data [2] = (m_time_remaining_write  & 0x00FF);
                data [3] = static_cast <unsigned char> (m_brightness_write);
                data [4] = 0;
                set_req_data (5, data);

//                 display_request ();
                write_frame ();
                omni_thread::sleep (0, 100000000);
                read_frame ();

              break;
                
        //-----------------------------------------------------------------------------------------------
            case MSG_COMMAND_OFF:

                req_set_function_code (WRITE_PARAMETERS);
                data [0] = 0x00;
                data [1] = 0;
                data [2] = 0;
                data [3] = 0;
                data [4] = 0;
                set_req_data (5, data);

                write_frame ();
                omni_thread::sleep (0, 100000000);
                read_frame ();

              break;
                
        //-----------------------------------------------------------------------------------------------
            case MSG_SET_BRIGHTNESS:
                m_brightness_write = _msg.get_data<unsigned short>(); 
                
                req_set_function_code (WRITE_PARAMETERS);
                req_set_data_length (0x05);

                data [0] = 0x01;
                data [1] = ((m_time_remaining_write  & 0xFF00) >> 8);
                data [2] = (m_time_remaining_write  & 0x00FF);
                data [3] = static_cast <unsigned char> (m_brightness_write);
                data [4] = 0;
                set_req_data (5, data);
                
                write_frame ();
                omni_thread::sleep (0, 100000000);
                read_frame ();

              break;
                
        //-----------------------------------------------------------------------------------------------
            case MSG_SET_TIME_MN:
                m_time_remaining_write = _msg.get_data<unsigned short>(); 
                
                req_set_function_code (WRITE_PARAMETERS);
                req_set_data_length (0x05);

                data [0] = 0x01;
                data [1] = ((m_time_remaining_write  & 0xFF00) >> 8);
                data [2] = (m_time_remaining_write  & 0x00FF);
                data [3] = static_cast <unsigned char> (m_brightness_write);
                data [4] = 0;
                set_req_data (5, data);

                write_frame ();
                omni_thread::sleep (0, 100000000);
                read_frame ();

              break;
                
        //-----------------------------------------------------------------------------------------------
            //- EXIT CASE
            case yat::TASK_EXIT:

              break;
        }//- End of switch
    }//- End of try
    catch( const Tango::DevFailed& df )
    {
//         _TANGO_TO_YAT_EXCEPTION( df, ex );
//         YAT_LOG_EXCEPTION( ex );
    }
    catch( const yat::Exception& e)
    {
//         YAT_LOG_EXCEPTION( e );
    }
    catch( const std::exception& e )
    {
//         YAT_LOG_ERROR( e.what() );
        //throw;
    }
    catch (...)
    {
        ERROR_STREAM << "DG645Task::process_message : Initialization Failed : caught [UNKNOWN]" << endl;
    }
}

//-----------------------------------------
//- Vch01Protocol::on
//-----------------------------------------
void Vch01Protocol::on ()
{
  std::vector<short> data;
  yat::Message * msg = new yat::Message(MSG_COMMAND_ON, MAX_USER_PRIORITY);
  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "Vch01Protocol::on");
  }
  
  //- don't wait till the message is processed !!
  post(msg);
}

//-----------------------------------------
//- Vch01Protocol::off
//-----------------------------------------
void Vch01Protocol::off (void)
{
  yat::Message * msg = new yat::Message(MSG_COMMAND_OFF, MAX_USER_PRIORITY);
  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "Vch01Protocol::off");
  }
  
  //- don't wait till the message is processed !!
  post(msg);
}

//-----------------------------------------
//- Vch01Protocol::update_device_status
//-----------------------------------------
void Vch01Protocol::update_device_status (void)
{
  yat::Timer t;

  req_set_function_code (GET_DEVICE_STATUS);
  unsigned char data [4];
  data [0] = 0x00;
  data [1] = 0x00;
  data [2] = 0x00;
  data [3] = 0x00;
  set_req_data (4, data);
  
  write_frame ();

  //- 10 ms delay between write/read?
  omni_thread::sleep (0, 1000000000);
  int nb_chars = read_frame ();

  //- try to read data
  if (nb_chars < DATA_START_ADDR_BYTE_OFFSET + 2)
  {
    return;
  }
  m_brightness_read = static_cast <unsigned short> (response [DATA_START_ADDR_BYTE_OFFSET + 2]);
  m_time_remaining_read = (static_cast <unsigned short> (response [DATA_START_ADDR_BYTE_OFFSET + 1]));
  m_time_remaining_read += (static_cast <unsigned short> (response [DATA_START_ADDR_BYTE_OFFSET]) * 256) ;
}
	
//-----------------------------------------
//- Vch01Protocol::display_request
//-----------------------------------------
void Vch01Protocol::display_request (void)
{
  std::size_t length = (7 + request [DATA_LEN_BYTE_OFFSET] + 1);
  std::clog << "<";
  for (std::size_t i = 0; i < length; i++)
  {
    std::clog << std::setfill ('0') << std::setw (2)  << static_cast <unsigned short>(request [i]);
    std::clog << " ";
  }
  std::clog << ">" << std::endl;
}

//-----------------------------------------
//- Vch01Protocol::display_response
//-----------------------------------------
void Vch01Protocol::display_response (void)
{
  std::size_t length = (7 + request [DATA_LEN_BYTE_OFFSET] + 1);
  std::clog << "<";
  for (size_t i = 0; i < length; i++)
  {
    std::clog << std::setfill ('0') << std::setw (2)  << static_cast <unsigned short>(response[i]);
    std::clog << " ";
  }
  std::clog << ">" << std::endl;
}
  
//-----------------------------------------
//- Vch01Protocol::is_on
//-----------------------------------------
bool Vch01Protocol::is_on (void)
{
  bool argout = false;
  
  if (m_time_remaining_read > 0)
  {
    argout = true;
  }
  return argout;
}

//-----------------------------------------
//- Vch01Protocol::get_brightness
//-----------------------------------------
unsigned short Vch01Protocol::get_brightness ()
{
  return static_cast <short> (m_brightness_read);
}

//-----------------------------------------
//- Vch01Protocol::get_time_mn
//-----------------------------------------
unsigned short Vch01Protocol::get_time_mn ()
{
  return static_cast <short> (m_time_remaining_read);
}

//-----------------------------------------
//- Vch01Protocol::set_brightness
//-----------------------------------------
void Vch01Protocol::set_brightness (unsigned short val)
{
  //- prepare msg
  yat::Message * msg = new yat::Message(MSG_SET_BRIGHTNESS, MAX_USER_PRIORITY);
  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "Vch01Protocol::set_brightness");
  }

  msg->attach_data(val);
  
  //- don't wait till the message is processed !!
  post(msg);
}

//-----------------------------------------
//- Vch01Protocol::set_time_mn
//-----------------------------------------
void Vch01Protocol::set_time_mn (unsigned short val)
{
  //- prepare msg
  yat::Message * msg = new yat::Message(MSG_SET_TIME_MN, MAX_USER_PRIORITY);
  if ( !msg )
  {
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "Vch01Protocol::set_time_mn");
  }

  msg->attach_data(val);
  
  //- don't wait till the message is processed !!
  post(msg);
}

//-----------------------------------------
//- Vch01Protocol::write_frame
//-----------------------------------------
void Vch01Protocol::write_frame (void)
{
  try
  {
    Tango::DeviceData ddflush;
    ddflush <<  static_cast <Tango::DevLong> (2);
    m_serial->command_inout ("DevSerFlush", ddflush);
    
    Tango::DevVarCharArray dvcain;
    req_set_device_group (0x99);

    std::size_t data_size = get_req_length ();
    dvcain.length (data_size);
    for (std::size_t i = 0; i < data_size; i++)
    {
      dvcain[i] = request [i];
    }
    
    Tango::DeviceData dd;
    dd << dvcain;
    m_serial->command_inout ("DevSerWriteChar",dd);
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << "Vch01Protocol::write_frame failed!" << std::endl;
    ERROR_STREAM << df << std::endl;
    m_error = std::string (df.errors[0].desc);
  }
}

//-----------------------------------------
//- Vch01Protocol::read_frame
//-----------------------------------------
int Vch01Protocol::read_frame (void)
{
  Tango::DevLong nb_char = 0;
  Tango::DeviceData ddout_nchar;
  std::vector <unsigned char> vc;
  Tango::DeviceData ddout_resp;

  try
  {
    ddout_nchar = m_serial->command_inout ("DevSerGetNChar");
    ddout_nchar >> nb_char;

    //- no response from controller
    if (nb_char <= 0)
    {
      return 0;
    }

    ddout_resp = m_serial->command_inout ("DevSerReadNBinData", ddout_nchar);
    ddout_resp >> vc;
    
    ddout_nchar >> nb_char;

    if (vc.size () != nb_char)
    {
      std::clog << "\t\tVch01Protocol::read_frame mismatch dvcaout.length () <" << vc.size () 
        << "> nbchars <" << nb_char << ">" << std::endl;
    }
    
    for (size_t i = 0; i < vc.size (); i++)
    {
      response [i] = static_cast <unsigned char> (vc [i]);
    }
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << "Vch01Protocol::read_frame failed!" << std::endl;
    ERROR_STREAM << df << std::endl;
    m_error = std::string (df.errors[0].desc);
  }

  return vc.size ();
}

} //- namespace
