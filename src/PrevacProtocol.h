//- Project : PREVAC Protocol class

#ifndef __PREVAC_PROTOCOL_H__
#define __PREVAC_PROTOCOL_H__

#include <vector>

namespace prevac
{

  //- Configuration structure
  typedef struct Config
  {
    Config ();
    Config (const Config & cnf);
    unsigned char header;
    unsigned char data_length;
    unsigned char device_address;
    unsigned char device_group;
    unsigned char logic_group;
    unsigned char driver_address;
    unsigned char function_code;
  }Config;


  typedef enum FunctionCode
  { 
    WRITE_PARAMETERS  = 0x10,
    START             = 0x47,
    EMERGENCY_STOP,
    INIT_DEVICE,
    STOP              = 0x4B,
    RESET_DEVICE      = 0x52,
    GET_DEVICE_STATUS,
    GET_LAST_ERROR,
    SET_COMM_ADDRESS  = 0x58,
    SET_LOGICAL_GROUP,
    READ_SERIAL_NUMBER = 0xFE,
    READ_DRIVER_VERSION
  }FunctionCode;

#define HEADER_BYTE_OFFSET 0
#define DATA_LEN_BYTE_OFFSET 1
#define REQUEST_DEV_ADDR_BYTE_OFFSET 2
#define RESPONS_DEV_ADDR_BYTE_OFFSET 5
#define DEV_GROUP_BYTE_OFFSET 3
#define LOG_GROUP_BYTE_OFFSET 4
#define REQUEST_DRV_ADDR_BYTE_OFFSET 5
#define RESPONS_RRV_ADDR_BYTE_OFFSET 2
#define FUNC_CODE_BYTE_OFFSET 6
#define DATA_START_ADDR_BYTE_OFFSET 7

  class PrevacProtocol
  {
  public :
    //- manufacturer default values applied  : device address = 0xC8
    //-                                        logic group    = 0xC8
    //- default values at CTOR               : function code  = 0xFF
    //-                                        data size      = 0x04
    //- manufacturer standard values applied : driver address = 0x01
    //-                                        protocol header= 0xAA
    PrevacProtocol (const Config & conf);
    virtual ~PrevacProtocol ();

  protected :

    int get_req_length (void);

    void get_response (std::vector <unsigned char> & data);

    void set_req_data (unsigned char len, unsigned char * data);
    
    void req_set_function_code (FunctionCode fc)
    {
      request [FUNC_CODE_BYTE_OFFSET] = static_cast <unsigned char> (fc);
    }
    void req_set_device_address (unsigned char addr)
    {
      request [REQUEST_DEV_ADDR_BYTE_OFFSET] = addr;
    }
    void req_set_data_length (unsigned char datl)
    {
      request [DATA_LEN_BYTE_OFFSET] = datl;
    }
    void req_set_device_group (unsigned char devgrp)
    {
      request [DEV_GROUP_BYTE_OFFSET] = devgrp;
    }
    void req_set_logic_group (unsigned char loggrp)
    {
      request [LOG_GROUP_BYTE_OFFSET] = loggrp;
    }

    unsigned char request  [8 + 256];
    unsigned char response [8 + 256];
    Config conf;
  };

}//- namespace prevac

#endif //-__PREVAC_PROTOCOL_H__
